/**
 * 
 */
package com.reptile.core.webmagic.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * @author wangzihang
 *
 */
public class HttpTaobao {

	/**
	 * 导出excel
	 * 
	 * @author wangzihang
	 */
	public static void expXlsQuery(List<Map<String, Object>> list) {
		WritableWorkbook book = null;
		try {
			book = Workbook.createWorkbook(new File("D:/category.xls"));
			WritableSheet sheet = book.createSheet("category" + new Date(), 0);
			WritableFont font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat format = new WritableCellFormat(font);

			// 标题行
			String[] titleArr = { "序号", "cid", "is_parent", "name", "parent_cid", "status" };
			for (int i = 0; i < titleArr.length; i++) {
				Label titleLabel = new Label(i, 0, titleArr[i], format);
				sheet.addCell(titleLabel);
			}

			// NumberFormat nf = new NumberFormat("0.00"); // 设置数字格式
			// WritableCellFormat wcfN = new WritableCellFormat(nf); // 设置表单格式
			// 添加列值
			for (int i = 0; i < list.size(); i++) {
				sheet.addCell(new Label(0, i + 1, String.valueOf(i + 1)));
				sheet.addCell(new Label(1, i + 1, list.get(i).get("cid").toString()));
				sheet.addCell(new Label(2, i + 1, list.get(i).get("is_parent").toString()));
				sheet.addCell(new Label(3, i + 1, list.get(i).get("name").toString()));
				sheet.addCell(new Label(4, i + 1, list.get(i).get("parent_cid").toString()));
				sheet.addCell(new Label(5, i + 1, list.get(i).get("status").toString()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (book == null) {
				return;
			}
			try {
				book.write();
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Boolean isRequestSuccessful(HttpResponse httpresponse) {
		return httpresponse.getStatusLine().getStatusCode() == 200;
	}

	/**
	 * 请求二级分类
	 */
	public static String HttpGet(String url, String cookie, String userAgent) throws Exception {
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Accept", "*/*");
		httpGet.addHeader("Accept-Encoding", "gzip, deflate");
		httpGet.addHeader("Accept-Language", "zh-CN,zh;q=0.9");
		httpGet.addHeader("Connection", "keep-alive");
		httpGet.addHeader("Cookie", cookie);
		httpGet.addHeader("Host", "open.taobao.com");
		httpGet.addHeader("Referer", "http://open.taobao.com/apitools/apiPropTools.htm?spm=0.0.0.0.mlPbbQ");
		httpGet.addHeader("User-Agent", userAgent);

		HttpClient httpClient = HttpClients.createDefault();
		HttpResponse httpresponse = null;
		try {
			httpresponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpresponse.getEntity();
			String response = EntityUtils.toString(httpEntity, "utf-8");
			return response;
		} catch (ClientProtocolException e) {
			System.out.println("http请求失败，uri{},exception{}");
		} catch (IOException e) {
			System.out.println("http请求失败，uri{},exception{}");
		}
		return null;
	}

	/**
	 * @author wangzihang
	 */
	public static void main(String[] args) {
		
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				System.setProperty("webdriver.chrome.driver",
						"H:/Users/wang/AppData/Local/Google/Chrome/Application/chromedriver.exe");// chromedriver服务地址
				WebDriver driver = new ChromeDriver(); // 新建一个WebDriver
														// 的对象，但是new
														// 的是FirefoxDriver的驱动
				driver.get("http://open.taobao.com/apitools/apiPropTools.htm?spm=0.0.0.0.mlPbbQ");// 打开指定的网站
				// driver.
				driver.findElement(By.className("J_Quick2Static")).click();
				driver.findElement(By.id("TPL_username_1")).sendKeys(new String[] { "" });// 
				driver.findElement(By.id("TPL_password_1")).sendKeys(new String[] { "" });
				driver.findElement(By.id("J_SubmitStatic")).click(); // 点击按扭

				try {
					/**
					 * WebDriver自带了一个智能等待的方法。
					 * dr.manage().timeouts().implicitlyWait(arg0, arg1）；
					 * Arg0：等待的时间长度，int 类型 ； Arg1：等待时间的单位 TimeUnit.SECONDS
					 * 一般用秒作为单位。
					 */
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					while (true) {
						driver.navigate().refresh();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 * dr.quit()和dr.close()都可以退出浏览器,简单的说一下两者的区别：第一个close，
				 * 如果打开了多个页面是关不干净的，它只关闭当前的一个页面。第二个quit，
				 * 是退出了所有Webdriver所有的窗口，退的非常干净，所以推荐使用quit最为一个case退出的方法。
				 */
				// driver.quit();//退出浏览器

			}
		});

		thread.start();
		
		
		// 导出excel数据存放地点
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		// 顶级分类数量
		int parentSize = 0;
		// 二级分类数量
		int twolevelSize = 0;
		// 三级分类数量
		int threelevelSize = 0;

		// 统计三级分类下时候还有四级分类
		int is_parent = 0;

		// 获取顶级分类，本地json
		try {
			File filePath = new File("D:/taobaoCategory.json");
			// 读取文件
			String input = FileUtils.readFileToString(filePath, "UTF-8");

			// 将读取的数据转换为JSONObject
			JSONObject fileJsonObject = JSONObject.parseObject(input);
			// 得到顶级分类
			JSONArray parentArray = fileJsonObject.getJSONArray("iteam_cid");

			// 遍历JSONArray，转换格式。按按钮集合按模块（name）放入map中
			Iterator<Object> parentIt = parentArray.iterator();

			// 遍历顶级分类开始
			while (parentIt.hasNext()) {
				String userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0";
				String cookie = "miid=168704831389634083; t=41f67944a12e9969952928576433d6e8; hng=CN%7Czh-CN%7CCNY%7C156; cna=ZrFQFMoRJg0CAXzsknUn4osN; tg=0; thw=cn; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0%26__ll%3D-1; tracknick=%5Cu6253%5Cu4E2A%5Cu6298%5Cu6263%5Cu5427%5Cu8001%5Cu677F; lgc=%5Cu6253%5Cu4E2A%5Cu6298%5Cu6263%5Cu5427%5Cu8001%5Cu677F; mt=ci=0_0&np=; v=0; cookie2=263286c138def600b48c1d301dc49b18; _tb_token_=38811abb6b86a; unb=1953598004; uc1=cookie16=WqG3DMC9UpAPBHGz5QBErFxlCA%3D%3D&cookie21=URm48syIYB3rzvI4Dim4&cookie15=WqG3DMC9VAQiUQ%3D%3D&existShop=false&pas=0&cookie14=UoTYNOgSn5NugQ%3D%3D&tag=8&lng=zh_CN; sg=%E6%9D%BF44; _l_g_=Ug%3D%3D; skt=6ff45e8f381fac6e; cookie1=B0f27vpFGP4y9D2e%2BCE9z5ZBSansgOiDnBz5PUG0FNY%3D; csg=1d2416f9; uc3=vt3=F8dByR6vCKyvIo4GG1I%3D&id2=UojRY%2BjEQh%2BUVg%3D%3D&nk2=1z78Lj9jugNephaFUMA%3D&lg2=UtASsssmOIJ0bQ%3D%3D; existShop=MTU0Mjc4OTQ2Mw%3D%3D; _cc_=W5iHLLyFfA%3D%3D; dnk=%5Cu6253%5Cu4E2A%5Cu6298%5Cu6263%5Cu5427%5Cu8001%5Cu677F; _nk_=%5Cu6253%5Cu4E2A%5Cu6298%5Cu6263%5Cu5427%5Cu8001%5Cu677F; cookie17=UojRY%2BjEQh%2BUVg%3D%3D; apush82a8011bdda7ee7b49da1e5553f42c44=%7B%22ts%22%3A1542789486975%2C%22parentId%22%3A1542789475966%7D; isg=BFdXf8XXwFvsz0TrSt2ZnUb55sthNCtR2R1-LamE9SaN2HUasWxgTrG-PjjjMAN2";
				
				JSONObject parentJson = (JSONObject) parentIt.next();
				Map<String, Object> parentMap = new HashMap<String, Object>();
				parentMap.put("parent_cid", parentJson.get("parent_cid"));
				parentMap.put("name", parentJson.get("name"));
				parentMap.put("is_parent", parentJson.get("is_parent"));
				parentMap.put("cid", parentJson.get("cid"));
				parentMap.put("status", parentJson.get("status"));
				list.add(parentMap);
				parentSize++;

				System.out.println("parent_cid:" + parentJson.get("parent_cid") + " name:" + parentJson.get("name")
						+ " is_parent:" + parentJson.get("is_parent") + " cid:" + parentJson.get("cid") + " status:"
						+ parentJson.get("status"));

				// 非顶级分类跳过
				if (parentJson.get("is_parent").toString().equals("false")) {
					continue;
				}

				String Url = "http://open.taobao.com/apitools/ajax_props.do?_tb_token_=38811abb6b86a&cid="
						+ parentJson.get("cid").toString()
						+ "&act=childCid&restBool=false&ua=090%23qCQXjvXUXqDXPXi0XXXXXQkOOHfeT97cf%2FX7IFZ2AGBODrVZcPj5Aw163oplk9GAq4QXiP%2B0gzvWXvXBV7V8i8N3oVMhExDuYk3G4kYEXvXQXXXXiPDiXXF2mp%2F9vQjBXvX6xDkZIE68N9k0PfDkXvXuLWQ5HfD5H4QXa67Mf8f56kKi%2F4QXU6hnXXa3HoQCh9T4tn73Oc7eG2XPHYVyrFhnLXj3HoD8h9k4aP73IfDtXvXQjsAKIUliXX8pDala%2F7nCELx%3D";
				// http://open.taobao.com/apitools/ajax_props.do?_tb_token_=38811abb6b86a&cid=16&act=childCid&restBool=false&ua=090%23qCQXjvXUXqDXPXi0XXXXXQkOOHfeT97cf%2FX7IFZ2AGBODrVZcPj5Aw163oplk9GAq4QXiP%2B0gzvWXvXBV7V8i8N3oVMhExDuYk3G4kYEXvXQXXXXiPDiXXF2mp%2F9vQjBXvX6xDkZIE68N9k0PfDkXvXuLWQ5HfD5H4QXa67Mf8f56kKi%2F4QXU6hnXXa3HoQCh9T4tn73Oc7eG2XPHYVyrFhnLXj3HoD8h9k4aP73IfDtXvXQjsAKIUliXX8pDala%2F7nCELx%3D

				// 实时变换userAgent,防止速度变慢
//				if (parentSize % 3 == 0) {
//					userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36";
//				} else if (parentSize % 3 != 0 && parentSize % 5 == 0) {
//					userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.26 Safari/537.36 Core/1.63.6756.400 QQBrowser/10.3.2473.400";
//				}

				// 请求分类数据
				String result_url_category = HttpGet(Url, cookie, userAgent);

				// System.out.println("-----------" + result_url_category +
				// "-----------");

				// 获取二级分类
				JSONObject twolevel_iteamcats_get_response = JSONObject.parseObject(result_url_category);
				JSONObject twolevel_iteam_cids = JSONObject.parseObject(twolevel_iteamcats_get_response
						.getJSONObject("itemcats_get_response").get("item_cats").toString());
				JSONArray twolevel_item_cat = twolevel_iteam_cids.getJSONArray("item_cat");

				Iterator<Object> twolevelIt = twolevel_item_cat.iterator();
				while (twolevelIt.hasNext()) {
					JSONObject twoLevelJson = (JSONObject) twolevelIt.next();
					Map<String, Object> twolevelMap = new HashMap<String, Object>();
					twolevelMap.put("parent_cid", twoLevelJson.get("parent_cid"));
					twolevelMap.put("name", twoLevelJson.get("name"));
					twolevelMap.put("is_parent", twoLevelJson.get("is_parent"));
					twolevelMap.put("cid", twoLevelJson.get("cid"));
					twolevelMap.put("status", twoLevelJson.get("status"));
					list.add(twolevelMap);
					twolevelSize++;

					System.out.println("twoLevel_parent_cid:" + twoLevelJson.get("parent_cid") + " twoLevel_name:"
							+ twoLevelJson.get("name") + " twoLevel_is_parent:" + twoLevelJson.get("is_parent")
							+ " twoLevel_cid:" + twoLevelJson.get("cid") + " twoLevel_status:"
							+ parentJson.get("status"));

					// 有父类接着请求下级分类
					if (twoLevelJson.get("is_parent").toString().equals("true")) {

						String threeUrl = "http://open.taobao.com/apitools/ajax_props.do?_tb_token_=38811abb6b86a&cid="
								+ twoLevelJson.get("cid").toString()
								+ "&act=childCid&restBool=false&ua=090%23qCQXjvXUXqDXPXi0XXXXXQkOOHfeT97cf%2FX7IFZ2AGBODrVZcPj5Aw163oplk9GAq4QXiP%2B0gzvWXvXBV7V8i8N3oVMhExDuYk3G4kYEXvXQXXXXiPDiXXF2mp%2F9vQjBXvX6xDkZIE68N9k0PfDkXvXuLWQ5HfD5H4QXa67Mf8f56kKi%2F4QXU6hnXXa3HoQCh9T4tn73Oc7eG2XPHYVyrFhnLXj3HoD8h9k4aP73IfDtXvXQjsAKIUliXX8pDala%2F7nCELx%3D";
						// http://open.taobao.com/apitools/ajax_props.do?_tb_token_=eb7e476a37f13&cid=120860012&act=childCid&restBool=false&ua=090%23qCQXGTXqXqYXPXi0XXXXXQkOOHfeH0hszlYm3eGXAGBvfHOZhPmnGw8PIzu7H95nq4QXius%2BSbQWXvXK777uiTN3oVMheVnuYk3G4kVTIo7qRD8q2oliXXz7MjaTq4QXius%2BSbu%2FXvXuXiauylXTPvQXit2Cqnl8qnViXXHXP6Wv4A7B7iLiXajeGXXXHYVCOFhnOUm3Ho6Mh9k4aP73IvZeG2XPHYVyqFhnLXj3HoD8wvQXudeRL7sx2DXh15NlSvyFOg4HoqfEQBEVxRn%3D
						
//						if (twolevelSize % 3 == 0) {
//							userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36";
//						} else if (twolevelSize % 3 != 0 && twolevelSize % 5 == 0) {
//							userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.26 Safari/537.36 Core/1.63.6756.400 QQBrowser/10.3.2473.400";
//						}
						
						String threelevel_result_url_category = HttpGet(threeUrl, cookie, userAgent);

						// 得到三级分类
						JSONObject threelevel_iteamcats_get_response = JSONObject
								.parseObject(threelevel_result_url_category);
						JSONObject threelevel_iteam_cids = JSONObject.parseObject(threelevel_iteamcats_get_response
								.getJSONObject("itemcats_get_response").get("item_cats").toString());
						JSONArray threelevel_item_cat = threelevel_iteam_cids.getJSONArray("item_cat");

						// 三级分类遍历开始
						Iterator<Object> threelevelIt = threelevel_item_cat.iterator();
						while (threelevelIt.hasNext()) {
							JSONObject threelevelJson = (JSONObject) threelevelIt.next();
							Map<String, Object> threeLevelMap = new HashMap<String, Object>();
							threeLevelMap.put("parent_cid", threelevelJson.get("parent_cid"));
							threeLevelMap.put("name", threelevelJson.get("name"));
							threeLevelMap.put("is_parent", threelevelJson.get("is_parent"));
							threeLevelMap.put("cid", threelevelJson.get("cid"));
							threeLevelMap.put("status", threelevelJson.get("status"));
							list.add(threeLevelMap);
							threelevelSize++;

							System.out.println("threeLevel_parent_cid:" + threelevelJson.get("parent_cid")
									+ " threeLevel_name:" + threelevelJson.get("name") + " threeLevel_is_parent:"
									+ threelevelJson.get("is_parent") + " threeLevel_cid:" + threelevelJson.get("cid")
									+ " threeLevel_status:" + threelevelJson.get("status"));

							if (threelevelJson.get("is_parent").toString().equals("true")) {
								System.out.println("存在四级分类");
								is_parent++;
							}

						}
					}
				}

				System.out.println("parentSize: " + parentSize);
				System.out.println("twolevelSize: " + twolevelSize);
				System.out.println("threelevelSize: " + threelevelSize);
				System.out.println("四级分类: " + is_parent);
			}

			thread.stop();
			// 导出excel
			expXlsQuery(list);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
