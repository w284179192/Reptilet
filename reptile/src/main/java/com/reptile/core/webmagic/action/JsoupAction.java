/**
 * 
 */
package com.reptile.core.webmagic.action;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author wangzihang
 *
 */
public class JsoupAction {

	/**
	 * @author wangzihang
	 */
	public static void main(String[] args) {

		// 计时
		long startTime, endTime;
		System.out.println("开始爬取...");

		try {
			// 爬取网站
			String URL = "https://www.oschina.net/blog?classification=428640";
			Document doc = Jsoup.connect(URL).get();

			//元素节点
			Elements newsHeadlines = doc.select(".blog-item");
			for (Element headline : newsHeadlines) {
				System.out.println(headline.attr("data-id"));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// 耗时
			startTime = System.currentTimeMillis();
			endTime = System.currentTimeMillis();
			System.out.println("爬取结束，耗时约" + ((endTime - startTime) / 1000) + "秒");
		}

	}

}
