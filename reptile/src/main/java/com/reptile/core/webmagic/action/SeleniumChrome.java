/**
 * 
 */
package com.reptile.core.webmagic.action;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author wangzihang
 *
 */
public class SeleniumChrome {

	/**
	 * @author wangzihang
	 */
	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				System.setProperty("webdriver.chrome.driver",
						"H:/Users/wang/AppData/Local/Google/Chrome/Application/chromedriver.exe");// chromedriver服务地址
				WebDriver driver = new ChromeDriver(); // 新建一个WebDriver
				
				driver.get("http://open.taobao.com/apitools/apiPropTools.htm?spm=0.0.0.0.mlPbbQ");// 打开指定的网站
				// driver.
				driver.findElement(By.id("TPL_username_1")).sendKeys(new String[] { "" });
				driver.findElement(By.id("TPL_password_1")).sendKeys(new String[] { "" });
				driver.findElement(By.id("J_SubmitStatic")).click(); // 点击按扭

				try {
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					while (true) {
						driver.navigate().refresh();
					}
					/**
					 * WebDriver自带了一个智能等待的方法。
					 * dr.manage().timeouts().implicitlyWait(arg0, arg1）；
					 * Arg0：等待的时间长度，int 类型 ； Arg1：等待时间的单位 TimeUnit.SECONDS
					 * 一般用秒作为单位。
					 */
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 * dr.quit()和dr.close()都可以退出浏览器,简单的说一下两者的区别：第一个close，
				 * 如果打开了多个页面是关不干净的，它只关闭当前的一个页面。第二个quit，
				 * 是退出了所有Webdriver所有的窗口，退的非常干净，所以推荐使用quit最为一个case退出的方法。
				 */
				// driver.quit();//退出浏览器

			}
		});

		thread.start();
		thread.stop();
	}

}
