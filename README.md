# Reptilet

#### 项目介绍
第一次写爬虫，emmmm，简单粗暴的得到了想要的数据。。。但是必须的完善- -！
用来爬取淘宝分类ID的一个方法。虽然简单粗暴，需要完善。
淘宝分类ID是http请求链接方式获取。
Jsoup，Selenium，WebMagic，这些都有尝试，最后发现事情并没有那么简单。。。
淘宝的一级分类在官网能正常获取。。。然而想要下级分类突然发现找不到。。。大概是我菜把。。。
最后参考https://blog.csdn.net/zhengzizhi/article/details/80716608 这个博客上的去写了一个java的爬取，因为没有看过Python。

#### 软件架构
虽然使用的是springboot，但是全称使用的是main方法执行。
淘宝分类获取看HttpTaoBao.java。
其它类，均是尝试使用各种爬虫工具。


#### 使用说明
请注意请求间隔。